<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehiclesHasRepair extends Model
{
    use HasFactory;

    protected $table = 'vehicles_has_repair';
    protected $fillable = [
        'nome',
        'km_entrada',
        'status',
        'placa',
        'reclamacao',
        'tecnico',
        'obs',
    ];
}
